package pl.codev.currencies.dagger

import dagger.Component
import pl.codev.currencies.data.viewModels.CurrenciesViewModel
import javax.inject.Singleton

/**
 * Created by Radek on 2020-04-23.
 */
@Singleton
@Component(modules = [CurrenciesModule::class])
interface CurrenciesComponent {
    fun inject(viewModel: CurrenciesViewModel)
}