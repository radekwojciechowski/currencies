package pl.codev.currencies.dagger

import android.app.Application
import android.content.Context
import android.util.Log
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import pl.codev.currencies.data.repository.CurrencyDataRepository
import pl.codev.currencies.data.services.CurrenciesService
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by Radek on 2020-04-23.
 */
@Module
class CurrenciesModule(private val app: Application) {

    @Provides
    @Singleton
    fun provideContext(): Context = app

    @Provides
    @Singleton
    fun provideCurrencyDataRepository(context: Context): CurrencyDataRepository {
        return CurrencyDataRepository(context)
    }

    @Provides
    @Singleton
    fun provideGitUsersService(): CurrenciesService {
        val loggingInterceptor = HttpLoggingInterceptor {
            Log.i("Retrofit", it)
        }
        //because of 1 sec data request duration, logging is off
        //loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient().newBuilder()
            .addInterceptor(loggingInterceptor)
            .build()

        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(
                RxJava2CallAdapterFactory.create())
            .addConverterFactory(
                GsonConverterFactory.create())
            .baseUrl("https://hiring.revolut.codes/")
            .client(client)
            .build()

        return retrofit.create(CurrenciesService::class.java)
    }
}