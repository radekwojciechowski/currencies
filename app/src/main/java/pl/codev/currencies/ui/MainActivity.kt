package pl.codev.currencies.ui

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.main_activity.*
import pl.codev.currencies.data.adapters.CurrenciesAdapter
import pl.codev.currencies.data.viewModels.CurrenciesViewModel
import android.widget.Toast


/**
 * Created by Radek on 2020-04-23.
 */
class MainActivity : AppCompatActivity() {

    companion object {
        private val TAG: String = MainActivity::class.java.simpleName
    }

    private lateinit var currenciesViewModel: CurrenciesViewModel
    private lateinit var currenciesViewAdapter: CurrenciesAdapter

    private var errorToast: Toast? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(pl.codev.currencies.R.layout.main_activity)

        currenciesViewModel = CurrenciesViewModel.create(this, application)

        setupUI()
        bindData()
    }

    private fun setupUI() {
        currenciesViewAdapter = CurrenciesAdapter(currenciesViewModel)
        currencies_recycler_view.apply {
            setHasFixedSize(true)
            adapter = currenciesViewAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }

    }

    private fun bindData() {
        currenciesViewModel.uiData.observe(this, Observer {
            currenciesViewAdapter.notifyDataSetChanged()
        })

        currenciesViewModel.errorLiveEvent.observe(this, Observer {
            showError(it)
        })
    }

    private fun showError(error: String) {
        errorToast?.let {
            it.cancel()
        }

        errorToast = Toast.makeText(this, error, Toast.LENGTH_SHORT)
        errorToast?.show()
    }
}