package pl.codev.currencies

import android.app.Application
import pl.codev.currencies.dagger.DaggerCurrenciesComponent
import pl.codev.currencies.dagger.CurrenciesComponent
import pl.codev.currencies.dagger.CurrenciesModule

/**
 * Created by Radek on 2020-04-23.
 */
class CurrenciesApplication: Application() {

    lateinit var currenciesComponent: CurrenciesComponent

    override fun onCreate() {
        super.onCreate()

        currenciesComponent = DaggerCurrenciesComponent.builder()
            .currenciesModule(CurrenciesModule(this))
            .build()
    }
}