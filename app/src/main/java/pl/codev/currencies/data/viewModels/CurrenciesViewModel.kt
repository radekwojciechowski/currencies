package pl.codev.currencies.data.viewModels

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import pl.codev.currencies.CurrenciesApplication
import pl.codev.currencies.R
import pl.codev.currencies.data.models.CurrenciesModel
import pl.codev.currencies.data.repository.CurrencyDataRepository
import pl.codev.currencies.data.services.CurrenciesService
import retrofit2.Response
import java.math.RoundingMode
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Radek on 2020-05-05.
 */
class CurrenciesViewModel(application: Application) : AndroidViewModel(application) {

    @Inject
    lateinit var currenciesService: CurrenciesService
    @Inject
    lateinit var currencyDataRepository: CurrencyDataRepository

    private var disposables = CompositeDisposable()

    private var baseCurrency = "eur"
    private var baseCurrencyValue = "0"

    private var latestNetworkData = LinkedHashMap<String, Double>()
    @Synchronized get
    @Synchronized set

    var uiData = MutableLiveData<List<CurrenciesModel.CurrencyItem>>()
        @Synchronized get
        @Synchronized set

    val errorLiveEvent = MutableLiveData<String>()

    companion object {
        private val TAG: String = CurrenciesViewModel::class.java.simpleName

        fun create(owner: ViewModelStoreOwner, application: Application): CurrenciesViewModel {
            return ViewModelProvider(owner, object : ViewModelProvider.Factory {
                override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                    @Suppress("UNCHECKED_CAST")
                    return CurrenciesViewModel(application) as T
                }
            }).get(CurrenciesViewModel::class.java)
        }
    }

    init {
        (application as CurrenciesApplication).currenciesComponent.inject(this)
        uiData.value = listOf<CurrenciesModel.CurrencyItem>()
        startDataRefreshing()
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

    fun setValueToRecalculate(newValue: String) {
        baseCurrencyValue = newValue
        recalculateUIData()
    }

    fun getItemAtPosition(position: Int): CurrenciesModel.CurrencyItem? {
        return uiData.value?.getOrNull(position)
    }

    fun getItemsCount(): Int {
        return uiData.value?.let {
            it.size
        } ?: run {
            0
        }
    }

    fun moveItemToTop(itemPosition: Int) {
        uiData.value?.getOrNull(itemPosition)?.let {
            baseCurrency = it.name
            it.currentValue.toDoubleOrNull()?.let {
                baseCurrencyValue = it.toString()
            }
            latestNetworkData.remove(it.name)
            var mutableUIData = uiData.value?.toMutableList()
            mutableUIData?.add(0, mutableUIData?.removeAt(itemPosition))

            uiData.value = mutableUIData?.toList()
        }

    }

    private fun startDataRefreshing() {
        val disposable = Observable.interval(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::readData, this::processError)

        disposables.add(disposable)
    }

    private fun readData(second: Long) {
        val networkDisposable = currenciesService.getAllCurrencies(baseCurrency)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::processData, this::processError)

        disposables.add(networkDisposable)
    }

    private fun processData(response: Response<CurrenciesModel.CurrenciesResponse>) {

        if (response.isSuccessful) {
            response.body()?.let {
                latestNetworkData = it.rates
                uiData.value?.let {
                    recalculateUIData()
                }

            }
        }

    }

    private fun processError(throwable: Throwable) {
        Log.e(TAG, "Error: " + throwable.message)
        throwable.printStackTrace()
        if (throwable is UnknownHostException)
            errorLiveEvent.value = getApplication<CurrenciesApplication>().resources.getString(R.string.missing_net_message)
    }

    private fun recalculateUIData() {
        var networkData = LinkedHashMap(latestNetworkData)

        val disposable = Observable.fromIterable(networkData.entries)
            .map {CurrenciesModel.CurrencyItem(it.key,
                currencyDataRepository.getCurrencyFullName(it.key),
                (baseCurrencyValue.toDouble() * it.value).toBigDecimal().setScale(2, RoundingMode.HALF_EVEN).toString())}
            .toList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                it.add(0, CurrenciesModel.CurrencyItem(baseCurrency.toUpperCase(),
                    currencyDataRepository.getCurrencyFullName(baseCurrency.toUpperCase()),
                    baseCurrencyValue))
                uiData.value = it
            }, this::processError)

        disposables.add(disposable)
    }
}