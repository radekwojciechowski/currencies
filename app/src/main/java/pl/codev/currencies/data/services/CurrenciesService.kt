package pl.codev.currencies.data.services

import io.reactivex.Observable
import pl.codev.currencies.data.models.CurrenciesModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Radek on 2020-04-23.
 */
interface CurrenciesService {

    @GET("api/android/latest/")
    fun getAllCurrencies(@Query("base") base: String): Observable<Response<CurrenciesModel.CurrenciesResponse>>

}