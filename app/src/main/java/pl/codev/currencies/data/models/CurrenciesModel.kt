package pl.codev.currencies.data.models

/**
 * Created by Radek on 2020-04-23.
 */
object CurrenciesModel {
    data class CurrenciesResponse(val baseCurrency: String, val rates: LinkedHashMap<String, Double>)
    data class CurrencyItem(val name: String, val fullName: String, val currentValue: String)
    data class CurrencyFullName(val name: String)
}