package pl.codev.currencies.data.adapters

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import io.reactivex.schedulers.Schedulers
import pl.codev.currencies.R
import pl.codev.currencies.data.viewModels.CurrenciesViewModel
import kotlinx.android.synthetic.main.currencies_list_item.*
import kotlinx.android.synthetic.main.currencies_list_item.view.*

/**
 * Created by Radek on 2020-05-05.
 */
class CurrenciesAdapter(viewModel: CurrenciesViewModel): RecyclerView.Adapter<CurrenciesAdapter.CurrenciesViewHolder>() {

    private var viewModel: CurrenciesViewModel = viewModel

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrenciesViewHolder {
        var currencyRowView = LayoutInflater.from(parent.context).inflate(R.layout.currencies_list_item, parent, false)
        return CurrenciesViewHolder(currencyRowView, parent.context)
    }

    override fun getItemCount(): Int {
        return viewModel.getItemsCount()
    }

    override fun onBindViewHolder(holder: CurrenciesViewHolder, position: Int) {
        holder.bind(viewModel, position)
    }

    inner class CurrenciesViewHolder(view: View, context: Context): RecyclerView.ViewHolder(view) {
        private var currencyListItem: View = view
        private val context = context
        private var disposable: Disposable? = null

        init {
            currencyListItem.setOnClickListener(moveTop())
        }

        fun bind(viewModel: CurrenciesViewModel, position: Int) {
            viewModel.getItemAtPosition(position)?.let {
                currencyListItem.currency_shortcut_text.text = it.name

                currencyListItem.currency_entered_value.apply {
                    isEnabled = (position == 0)
                    disposable?.dispose()
                    setText(it.currentValue)

                    if (position == 0) {
                        setSelection(text.length)
                        setupEditTextField(this)
                    }

                }

                currencyListItem.currency_full_text.text = it.fullName

                context.resources.getIdentifier(it.name.toLowerCase(), "mipmap", context.packageName).takeIf { it > 0 }?.also {
                    Picasso.with(context)
                        .load(it)
                        .into(currencyListItem.currency_flag)
                }

            }
        }

        private fun moveTop(): (View) -> Unit = {
            layoutPosition.takeIf { it > 0 }?.also { currentPosition ->
                viewModel.moveItemToTop(currentPosition)
                notifyItemMoved(currentPosition, 0)
                currencyListItem.currency_entered_value.apply {
                    requestFocus()
                    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
                }

            }
        }

        private fun setupEditTextField(newEditText: EditText) {
            disposable?.dispose()
            disposable = textChange(newEditText)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    viewModel.setValueToRecalculate(it.toString())
                }
        }

        private fun textChange(editText: EditText): Observable<String> {
            return Observable.create<String> { emitter ->

                val textWatcher = object : TextWatcher {

                    override fun afterTextChanged(s: Editable?) = Unit

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

                    override fun onTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                        s?.toString()?.let { emitter.onNext(it) }
                    }
                }

                editText.addTextChangedListener(textWatcher)

                emitter.setDisposable(Disposables.fromAction {
                    editText.removeTextChangedListener(textWatcher) })
            }

        }
    }

}