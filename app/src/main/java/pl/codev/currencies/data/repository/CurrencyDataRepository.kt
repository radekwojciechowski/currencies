package pl.codev.currencies.data.repository

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import pl.codev.currencies.data.models.CurrenciesModel
import java.io.IOException

/**
 * Created by Radek on 2020-05-07.
 */
class CurrencyDataRepository(context: Context) {
    private val context = context
    private var currencyData: LinkedHashMap<String, CurrenciesModel.CurrencyFullName>? = null

    init {
        currencyData = getCurrencyData()
    }

    fun getCurrencyFullName(currencyCode: String): String {
        var fullName = ""
        currencyData?.let {
            it.get(currencyCode)?.let {
                fullName = it.name
            }
        }

        return fullName
    }

    private fun getCurrencyData(): LinkedHashMap<String, CurrenciesModel.CurrencyFullName>? {
        var currencyData: LinkedHashMap<String, CurrenciesModel.CurrencyFullName>? = null

        getJsonData()?.let {
            val gson = Gson()
            val currencyDataType = object : TypeToken<LinkedHashMap<String, CurrenciesModel.CurrencyFullName>>() {}.type

            currencyData = gson.fromJson(it, currencyDataType)
        }

        return currencyData
    }

    private fun getJsonData(): String? {
        var jsonString: String? = null

        try {
            jsonString = context.assets.open("currency.json").bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
        }

        return jsonString
    }
}